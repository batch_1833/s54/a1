let collection = [];

// Write the queue functions below.
// Note: Avoid using Array methods (except .length) on creating the queue functions.

function Queue() {
    this.collection = [];
    this.count =0;

    //To Print the collection in the console.
    this.print = function() { 
      console.log(this.collection)
    };

    //Add element to the end of the queue.
    this.enqueue = function(value) {
        this.collection[this.count++] = value
        
     };

    // To delete the first item in the collection.
    this.dequeue = function() { 
     
    };

    //Show the first element of the queue.
    this.front = function() { };

    //Show the number of elements in the queue.
    this.size = function() { 
      return this.count
    };

    //To check if the queue is empty or not.
    this.isEmpty = function() { 
      if(this.count === 0){
        return undefined
      }
    };
}

//instantiate an object from the satck class constructor
let myStack = new Queue()


module.exports = {
	//export created queue functions
};